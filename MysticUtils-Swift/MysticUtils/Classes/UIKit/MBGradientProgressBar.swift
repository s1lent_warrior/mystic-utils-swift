//
//  MBGradientProgressBar.swift
//  MysticUtils-Swift
//
//  Created by Muneeb Ahmed Anwar on 09/10/2016.
//  Copyright © 2016 MysticBots. All rights reserved.
//

import UIKit

@objc class MBGradientProgressBar: UIView {
    fileprivate let maskLayer = CALayer()
    
    override var frame: CGRect {
        didSet {
            self.setup()
        }
    }
    
    var progress: Float = 0.0 {
        didSet {
            if oldValue != progress {
                progress = (progress > 1.0) ? 1.0 : progress
                DispatchQueue.main.async {
                    self.setNeedsLayout()
                }
            }
        }
    }
    
    var isAnimating = false
    private (set) var colors : [CGColor] = [] {
        didSet {
            self.performAnimation()
        }
    }
    
    func setColors(colors: [CGColor]) {
        DispatchQueue.main.async {
            self.colors = colors
        }
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setup()
    }
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    fileprivate func setup() {
        let layer = self.layer as! CAGradientLayer
        layer.startPoint = CGPoint(x: 0.0, y: 0.5)
        layer.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        layer.colors = self.colors
        
        maskLayer.frame = CGRect(x: 0.0, y: 0.0, width: 0.0, height: frame.size.height)
        maskLayer.backgroundColor = UIColor.white.cgColor
        layer.mask = maskLayer
    }
    
    override func layoutSubviews() {
        var maskRect = maskLayer.frame
        maskRect.size.width = self.bounds.width * CGFloat(progress)
        maskLayer.frame = maskRect
    }
    
    func performAnimation() {
        let layer = self.layer as! CAGradientLayer
        let fromColors = layer.colors
        let toColors = self.colors
        layer.colors = toColors
        
        // Create an animation to slowly move the hue gradient left to right.
        let animation = CABasicAnimation(keyPath: "colors")
        animation.fromValue = fromColors
        animation.toValue = toColors
        animation.duration = 0.08
        animation.isRemovedOnCompletion = true
        animation.fillMode = kCAFillModeForwards
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        animation.delegate = self
        
        // Add the animation to our layer
        layer.add(animation, forKey: "animateGradient")
    }
    
    func startAnimating() {
        if !self.isAnimating {
            isAnimating = true
            self.performAnimation()
        }
    }
    
    func stopAnimating() {
        if self.isAnimating {
            isAnimating = false
        }
    }
    
    func reset() {
        DispatchQueue.main.async {
            self.colors = []
            self.progress = 0.0
            self.stopAnimating()
        }
    }
}

extension MBGradientProgressBar: CAAnimationDelegate {
    func animationDidStop(_ animation: CAAnimation, finished flag: Bool) {
        if self.isAnimating {
            self.performAnimation()
        }
    }
}
