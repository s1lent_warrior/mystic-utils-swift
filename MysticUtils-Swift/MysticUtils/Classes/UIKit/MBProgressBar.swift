//
//  MBProgressBar.swift
//  FocusBandTemplate
//
//  Created by Muneeb Ahmed Anwar on 25/06/2016.
//  Copyright © 2016 MysticBots. All rights reserved.
//

import UIKit

class MBProgressBar: UIView {
    var defaultColor = UIColor.clear
    
    fileprivate var indexPaths : [IndexPath] = []
    fileprivate var blockEnum: IndexingIterator<[IndexPath]>
    fileprivate var blocksCount: Int = 0
    fileprivate var currentBlock: Int = 0
    
    required init?(coder aDecoder: NSCoder) {
        blockEnum = IndexingIterator(_elements: indexPaths)
        super.init(coder: aDecoder)
    }
    
    weak var dataSource: MBProgressBarDataSource? {
        didSet {
            if dataSource == nil {
                self.reset()
            } else {
                self.resetProgressBar()
            }
        }
    }
    
    func setupProgressBar() {
        self.setupProgressBarWithColor(nil)
    }
    
    func setupProgressBarWithColor(_ color: UIColor?) {
        if color != nil {
            self.defaultColor = color!
        }
        
        indexPaths.removeAll()
        
        if let sections = dataSource?.numberOfSectionsInProgressBar(self) {
            for i in 0 ..< sections {
                let blocks = dataSource!.progressBar(self, numberOfBlocksInSection: i)
                blocksCount += blocks
                let width = self.frame.size.width / CGFloat(sections)
                let height = self.frame.size.height
                let viewSection = UIView(frame: CGRect(x: CGFloat(i) * width, y: 0, width: width, height: height))
                viewSection.backgroundColor = UIColor.clear
                self.addSubview(viewSection)
                for j in 0 ..< blocks {
                    let indexPath = IndexPath(item: j, section: i)
                    indexPaths.append(indexPath)
                }
            }
            
            blockEnum = indexPaths.makeIterator()
        }
        currentBlock = 0
    }
    
    func progress() {
        self.progressWithColor(self.defaultColor)
    }
    
    func progressWithColor(_ color: UIColor?) {
        if let dataSource_ = dataSource, let progressColor = (color != nil) ? color : self.defaultColor {
            if let indexPath = blockEnum.next() {
                let blocks = dataSource_.progressBar(self, numberOfBlocksInSection: (indexPath as NSIndexPath).section)
                let viewBlock = dataSource_.progressBar(self, viewForBlock: (indexPath as NSIndexPath).item, inSection: (indexPath as NSIndexPath).section)
                let viewSection = self.subviews[(indexPath as NSIndexPath).section]
                viewSection.addSubview(viewBlock)
                let width = viewSection.frame.size.width / CGFloat(blocks)
                let height = viewSection.frame.size.height
                viewBlock.frame = CGRect(x: CGFloat((indexPath as NSIndexPath).item) * width, y: 0.0, width: width, height: height)
                viewBlock.backgroundColor = color
                viewBlock.tag = currentBlock
                currentBlock += 1
            } else {
                if let shouldReset = dataSource_.progressBarShouldResetOnCompletion?(self) {
                    if shouldReset == true {
                        self.resetProgressBar()
                        self.progressWithColor(progressColor)
                    }
                }
            }
        }
    }
    
    func resetProgressBar() {
        DispatchQueue.main.async { 
            self.reset()
            self.setupProgressBar()
        }
    }
    
    func reset() {
        blocksCount = 0
        currentBlock = 0
        self.removeSubviewsFromView(self)
    }
    
    // Recursive
    func removeSubviewsFromView(_ view: UIView) {
        for subview: UIView in view.subviews {
            if !subview.subviews.isEmpty {
                self.removeSubviewsFromView(subview)
            }
            subview.removeFromSuperview()
        }
    }
    
    // Iterative
//    func removeSubviewsFromView(view: UIView) {
//        var views = [view]
//        while !views.isEmpty {
//            for subview: UIView in views.popLast()!.subviews {
//                if !subview.subviews.isEmpty {
//                    views.append(subview)
//                }
//                subview.removeFromSuperview()
//            }
//        }
//    }
}

@objc protocol MBProgressBarDataSource {
    func numberOfSectionsInProgressBar(_ progressBar: MBProgressBar) -> Int
    func progressBar(_ progressBar: MBProgressBar, numberOfBlocksInSection section: Int) -> Int
    func progressBar(_ progressBar: MBProgressBar, viewForBlock block: Int, inSection section: Int) -> UIView
    @objc optional func progressBarShouldResetOnCompletion(_ progressBar: MBProgressBar) -> Bool
}
