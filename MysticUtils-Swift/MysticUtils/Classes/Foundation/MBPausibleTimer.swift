//
//  MBPausibleTimer.swift
//  iFocusTour
//
//  Created by Muneeb Ahmed Anwar on 26/06/2016.
//  Copyright © 2016 MysticBots. All rights reserved.
//

import UIKit

class MBPausibleTimer: NSObject {
    weak var delegate: MBPausibleTimerDelegate?;
    
    private(set) var timeInterval: TimeInterval;
    private(set) var timer: Timer?;
    private(set) var isPaused = true;
    private(set) var timeElapsed: TimeInterval = 0;
    
    class func timerWithTimeInterval(timeInterval: TimeInterval, delegate: MBPausibleTimerDelegate) -> MBPausibleTimer {
        return MBPausibleTimer(timeInterval: timeInterval, delegate: delegate);
    }
    
    required init(timeInterval: TimeInterval, delegate: MBPausibleTimerDelegate) {
        self.timeInterval = timeInterval;
        self.delegate = delegate;
        
        super.init();
    }
    
    deinit {
        timeElapsed = 0;
        timer!.invalidate();
        timer = nil;
        delegate = nil;
        isPaused = true;
        timeInterval = 0;
    }
    
    func restart() {
        self.reset();
        self.resume();
    }
    
    func reset() {
        timer?.invalidate();
        timer = nil;
        timeElapsed = 0;
    }
    
    func resume() {
        if timer == nil {
            DispatchQueue.main.async(execute: {
                self.timer = Timer.scheduledTimer(timeInterval: self.timeInterval, target: self, selector: #selector(MBPausibleTimer.updateTimer), userInfo: nil, repeats: true);
            });
        }
        
        isPaused = false;
    }
    
    func pause() {
        isPaused = true;
        
        timer?.invalidate();
        timer = nil;
    }
    
    func updateTimer() {
        if isPaused == false {
            timeElapsed = (timeElapsed + 1) * timeInterval;
            self.delegate?.timer(timer: self, didUpdateWithTimeInterval: timeElapsed);
        }
    }
    
}

@objc protocol MBPausibleTimerDelegate {
    func timer(timer: MBPausibleTimer, didUpdateWithTimeInterval time: TimeInterval);
}
