//
//  UIImage.swift
//  iFocusTour
//
//  Created by Muneeb Ahmed Anwar on 18/09/2016.
//  Copyright © 2016 MysticBots. All rights reserved.
//

import UIKit
import AVFoundation

public extension UIImage {
    
    // MARK: - Class Methods
    public static func thumbImageFromMovie(atPath filepath: NSString, scaledTo size: CGSize) -> UIImage? {
        let url = URL(fileURLWithPath: filepath.expandingTildeInPath)
        let asset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        var time = asset.duration
        time.value = 100
        do {
            let imageRef = try imageGenerator.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: imageRef)
            if size.equalTo(CGSize.zero) {
                return thumbnail
            }
            
            return thumbnail.scaledImage(size)
            
        } catch (let error as NSError) {
            print(error.localizedDescription)
        }
        
        return nil;
    }
    
    public static func menuImage() -> UIImage? {
        var defaultImage : UIImage;
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 20, height: 13), false, 0.0);
        UIColor.black.setFill();
        UIBezierPath(rect: CGRect(x: 0, y: 0, width: 20, height: 1)).fill();
        UIBezierPath(rect: CGRect(x: 0, y: 5, width: 20, height: 1)).fill();
        UIBezierPath(rect: CGRect(x: 0, y: 10, width: 20, height: 1)).fill();
        UIColor.white.setFill();
        UIBezierPath(rect: CGRect(x: 0, y: 1, width: 20, height: 2)).fill();
        UIBezierPath(rect: CGRect(x: 0, y: 6, width: 20, height: 2)).fill();
        UIBezierPath(rect: CGRect(x: 0, y: 11, width: 20, height: 2)).fill();
        defaultImage = UIGraphicsGetImageFromCurrentImageContext()!;
        UIGraphicsEndImageContext();

        return defaultImage;
    }
    
    // MARK: - Instance Methods
    public func scaledImage(_ size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        self.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    public func compressed() -> UIImage {
        let cgImage = self.cgImage
        
        let width = cgImage!.width / 2
        let height = cgImage!.height / 2
        let bitsPerComponent = cgImage!.bitsPerComponent
        let bytesPerRow = cgImage!.bytesPerRow
        let colorSpace = cgImage!.colorSpace
        let bitmapInfo = cgImage!.bitmapInfo
        
        let context = CGContext(data: nil, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace!, bitmapInfo: bitmapInfo.rawValue)
        
        
        context!.interpolationQuality = .high
        
        context!.draw(cgImage!, in: CGRect(origin: CGPoint.zero, size: CGSize(width: CGFloat(width), height: CGFloat(height))))
        
        
        let scaledImage = context!.makeImage().flatMap {
            UIImage(cgImage: $0)
        }
        
        return scaledImage!;
    }
}
